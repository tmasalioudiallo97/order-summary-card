module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme => ({
        desktop: "url('../images/pattern-background-desktop.svg')",
        mobile: "url('../images/pattern-background-mobile.svg')"
      }),

      backgroundSize: {
        '1440': '1440px 300px',
        '375': '375px 300px'
      },
      backgroundPosition: {
        bposition: 'bottom 5px',
      },

      height: {
        '400': '400px'
      },

      spacing: {
        1440: '90rem',
      },
      fontFamily: {
        title: ["Red Hat Display"]
      },
      colors: {
        pale_blue: 'hsl(225, 100%, 94%)',
        bright_blue: 'hsl(245, 75%, 52%)',
        Very_pale_blue: 'hsl(225, 100%, 98%)',
        desaturated_blue: 'hsl(224, 23%, 55%)',
        dark_blue: 'hsl(223, 47%, 23%)',
        hover_color: 'hsl(245, 83%, 68%)',
        card_bg: 'hsl(0, 0%, 100%)'
      },
      boxShadow: {
        myshadow: '0 1.6rem 1rem 0.1rem hsl(245, 75%, 52%, 0.15)',
      }

    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
